import time
import datetime
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
# sphinx_gallery_thumbnail_number = 2
    
class expedia:
    def __init__(self, end, start, start_date_generated, end_date_generated, travelFrom, travelTo, stops, filterByDuration):
        self.start_date_generated = start_date_generated
        self.end_date_generated = end_date_generated
        self.end = end
        self.start = start
        self.travelFrom = travelFrom
        self.travelTo = travelTo
        self.bestData = [sys.maxint, ""]
        self.stops = stops
        self.filterByDuration = filterByDuration
        self.getData()

    def getData(self):
        print "Crawling expedia.co.uk in Progress"
        browser = webdriver.Chrome(executable_path='C:\Users\Hackky\Downloads\chromedriver_win32/chromedriver.exe')
        browser.get('https://www.expedia.co.uk/flights')

        element = browser.find_element_by_id("flight-returning-flp")
        endDate = datetime.datetime.strptime(str(self.end), "%d-%m-%Y")
        returnDate = endDate.strftime("%d/%m/%Y")
        element.send_keys(returnDate)

        element = browser.find_element_by_id("flight-departing-flp")
        startDate = datetime.datetime.strptime(str(self.start), "%d-%m-%Y")
        departDate = startDate.strftime("%d/%m/%Y")
        element.send_keys(departDate)

        element = browser.find_element_by_id("flight-origin-flp")
        element.click()
        element.send_keys(self.travelFrom)

        element = browser.find_element_by_id("flight-destination-flp")
        element.click()
        element.send_keys(self.travelTo)

        element = browser.find_element_by_xpath('/html/body/meso-native-marquee/section/div/div/div[1]/section/div[1]/div[2]/div[2]/section[1]/form/div[8]/label/button')
        element.click()


        flightMatrix = np.zeros((len(self.start_date_generated),len(self.end_date_generated)))

        xAxis = []
        yAxis = []
        ## loop to get xAxis and yAxis
        for date in self.start_date_generated:
            xAxis.append(date.strftime("%d/%m/%Y"))
        for date in self.end_date_generated:
            yAxis.append(date.strftime("%d/%m/%Y"))

            
        #loop to fetch data from expedia    
        for startIndex, date in enumerate(self.start_date_generated):
            for endIndex, enddate in enumerate(self.end_date_generated):
                departDate = date.strftime("%d/%m/%Y")
                returnDate = enddate.strftime("%d/%m/%Y")

                element = browser.find_element_by_id("return-date-1")
                element.clear()
                element.send_keys(returnDate)

                element = browser.find_element_by_id("departure-date-1")
                element.clear()
                element.send_keys(departDate)

                element = browser.find_element_by_id('flight-wizard-search-button')
                element.click()

                try:
                    element = WebDriverWait(browser, 20).until(
                           EC.presence_of_element_located((By.XPATH, "/html/body/div[2]/div[10]/section/div/div[11]/ul/li[1]/div[1]/div[1]/div[2]/div/div[1]/div[1]"))
                          )

                    #element = browser.find_element_by_id("airlines")

                    #browser.find_element_by_xpath("//input[@value='MH']").click()
                    if self.filterByDuration == 'y':
                        browser.find_element_by_xpath("/html/body/div[2]/div[10]/aside/div[2]/div[2]/div[1]/div/label/select/option[3]").click()
                    if self.stops == "1":
                        browser.find_element_by_xpath("/html/body/div[2]/div[10]/aside/div[2]/div[2]/div[3]/div/fieldset[1]/div/div[3]/div").click()
                    if self.stops == "2":
                        browser.find_element_by_xpath("/html/body/div[2]/div[10]/aside/div[2]/div[2]/div[3]/div/fieldset[1]/div/div[4]/div").click()
                    
                    element = browser.find_element_by_xpath('/html/body/div[2]/div[10]/section/div/div[11]/ul/li[1]/div[1]/div[1]/div[2]/div/div[1]/div[1]').text

                    price = element.split(unichr(163),1)[1]
                    #airlines = browser.find_element_by_xpath('/html/body/div[2]/div[10]/section/div/div[10]/ul/li[1]/div[1]/div[1]/div[1]/div/div/div/div[1]/div[2]').text
                    #transit = browser.find_element_by_xpath('/html/body/div[2]/div[10]/section/div/div[10]/ul/li[1]/div[1]/div[1]/div[1]/div/div/div/div[2]/div[2]').text
                    print departDate + " -- " + returnDate
                    print "Price :" + str(price)
                    #print "Airlines :" + str(airlines)
                    #print "Transit :" +str(transit)
                    print "---------------------"
                    flightMatrix[startIndex][endIndex] = price
                    if int(price) < self.bestData[0]:
                        self.bestData[0] = int(price)
                        bestDateRange = str(departDate) + "--" + str(returnDate)
                        self.bestData[1] = bestDateRange
                        print "Best Price : " + str(self.bestData[0]) + " // " +"Date range : " + str(self.bestData[1])
                except:
                    continue
                else:
                    continue
        final= "----- Final Best Price : " + str(self.bestData[0]) + " // " +"Date range : " + str(self.bestData[1])
        print final
        browser.quit()
        vegetables = xAxis
        farmers = yAxis

        harvest = flightMatrix

        fig, ax = plt.subplots()
        im = ax.imshow(harvest)

        # We want to show all ticks...
        ax.set_xticks(np.arange(len(farmers)))
        ax.set_yticks(np.arange(len(vegetables)))
        # ... and label them with the respective list entries
        ax.set_xticklabels(farmers)
        ax.set_yticklabels(vegetables)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(vegetables)):
            for j in range(len(farmers)):
                text = ax.text(j, i, harvest[i, j],
                               ha="center", va="center", color="w")

        #add text
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(5, 5, "test text", transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)

        ax.set_title("Expedia flights details")
        fig.tight_layout()
        plt.show()
        



        
