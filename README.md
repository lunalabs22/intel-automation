Expedia.co.uk flights crawler to find the cheapest price in given date ranges from source to destination and back.

INSTALL THIS BEFORE RUNNING
1. Install Python https://www.python.org/downloads/ , I have used Python 2.7 for this

2. Install PIP 
    * Download get-pip.py from https://bootstrap.pypa.io/get-pip.py to a folder on your computer.
    * Open a command prompt and navigate to the folder containing get-pip.py.
    * Run the following command:
    * python get-pip.py
    * Pip is now installed!

3. Get crawler essentials":
    * PIP install selenium
    * PIP install numpy
    * PIP install matplotlib
    
4. Download chrome driver for selenium 
    https://chromedriver.chromium.org/downloads

5. Locate this code inside module/expedia.py file :         
    browser = webdriver.Chrome(executable_path='C:\Users\Hackky\Downloads\chromedriver_win32/chromedriver.exe')
    and replace executable_path with the path where your chrome driver has been installed in your system.
 

    
You are now ready to run the tool.

Example inputs and outputs:

Let us suppose you want to go to kathmandu on 26/03/2020 and return to London on 07/04/2020
The tool will give you the best price before and after 3 days of departure and return.
At the end it will also make you a heatmap chart to show all the price combinations matrix for better visual representation.

Feel free to use!


