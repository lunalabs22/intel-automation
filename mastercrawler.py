import time
import datetime
from modules.expedia import expedia

def getInput(message):
    print message
    return raw_input()
    
def getStartDate():
    print "Please enter departure day, month & year"
    day = getInput("Enter Day of departure, Eg: 26")
    month = getInput("Enter Month of departure, Eg: 3")
    year = getInput("Enter Year of departure, Eg: 2020")
    return str(day)+ "-" + str(month)+ "-" + str(year)

def getReturnDate():
    print "Please enter return day, month & year"
    rDay = getInput("Enter Day of return, Eg: 7")
    rMonth = getInput("Enter Month of return, Eg: 4")
    rYear = getInput("Enter Year of return, Eg: 2020")
    return str(rDay)+ "-" + str(rMonth)+ "-" + str(rYear)

def calculateStartDateRanges(departureDate):
    start = datetime.datetime.strptime(str(departureDate), "%d-%m-%Y") - datetime.timedelta(days=3)
    start_date_generated = [start + datetime.timedelta(days=x) for x in range(0, 7)]
    return start_date_generated

def calculateReturnDateRanges(returnDate):
    end = datetime.datetime.strptime(str(returnDate), "%d-%m-%Y") - datetime.timedelta(days=3)
    end_date_generated = [end + datetime.timedelta(days=x) for x in range(0, 7)]
    return end_date_generated

def init():
    departureDate =  getStartDate()
    returnDate = getReturnDate()
    departureCountry = getInput("Enter Departure Country, Eg: kathmandu")
    destinationCountry = getInput("Enter Return Country, Eg: heathrow")
    departDateRanges = calculateStartDateRanges(departureDate)
    returnDateRanges = calculateReturnDateRanges(returnDate)
    noOfStops = getInput("Enter Number of Stops?, Eg: 1")
    filterByDuration = getInput("Filter by shortest duration? (Y for yes)")
    expedia(returnDate, departureDate, departDateRanges, returnDateRanges, departureCountry, destinationCountry, noOfStops, filterByDuration)

init()

    




